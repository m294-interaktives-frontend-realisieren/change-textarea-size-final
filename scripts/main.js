function changeTextareaSize() {
  const SIZE_VALUE_SMALL = 'small';
  const SIZE_VALUE_BIG = 'big';

  const COLS_SMALL = 20;
  const COLS_BIG = 40;
  const ROWS_SMALL = 10;
  const ROWS_BIG = 20;

  let elSelectedSize = document.querySelector('#size-form input:checked');
  let selectedValue = elSelectedSize.value;
  if (selectedValue === SIZE_VALUE_BIG) {
    setTextareaColsRows(COLS_BIG, ROWS_BIG);
  } else if (selectedValue === SIZE_VALUE_SMALL) {
    setTextareaColsRows(COLS_SMALL, ROWS_SMALL);
  }
}

// set cols and rows attribute of textarea
function setTextareaColsRows(cols, rows) {
  let elTextarea = document.getElementById('textarea');
  elTextarea.setAttribute('cols', cols);
  elTextarea.setAttribute('rows', rows);
  // Unten rechts kann man Grösse von Textfeld manuell anpassen
  // Dies wird über ein Attribut style gemacht, welches die Werte
  // in den Attributen cols und rows überschreibt. Deshalb
  // muss das Attribut style weggenommen werden damit cols und rows
  // beachtet wird, wenn man die Grösse des Textfeld angepasst hat.
  elTextarea.removeAttribute('style');
}
